# Proyecto MVC

Se agrega del proyecto anterior (DAO, Models y Controllers) los objetos que se usaron y ademas se crea la capa Vista y parte del controlador para su correcto uso y entendimiento.

## Instalación

Instalar en el IDE Eclipse el servidor de aplicaciones TOMCAT 9 
Para instalar las librerias en el IDE Eclipse debes ir al proyecto y hacer clic con el boton derecho, luego seleccionar Properties >> Java Build Path >> Libreries >> Classpath >> boton Add External JARs, y agregar las librerias (jar) que necesitas.

La libreria de conexion a la DB deben agregarla a la carpeta LIB de Tomcat.

## Clonar el proyecto
Para clonar el repositorio, utiliza el siguiente comando:

```bash
git clone https://gitlab.com/202405-alkemy-java-6/202405-115-116-mvc-ejemplo.git

