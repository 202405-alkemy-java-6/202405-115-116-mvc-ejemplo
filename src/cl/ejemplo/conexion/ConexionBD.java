package cl.ejemplo.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionBD {
    private String url = "jdbc:mysql://localhost:3306/shop";
    private String usuario = "root";
    private String pass = "root";


	public Connection establecerConexion() {
	       try {
	            Class.forName("com.mysql.cj.jdbc.Driver");
	            return DriverManager.getConnection(url, usuario, pass);
	        } catch (ClassNotFoundException | SQLException e) {
	            e.printStackTrace();
	            return null;
	        }
	}
}
