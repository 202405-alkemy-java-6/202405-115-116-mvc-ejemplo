package cl.ejemplo.controller;

import java.sql.Connection;
import java.util.Scanner;

import cl.ejemplo.conexion.ConexionBD;
import cl.ejemplo.dao.Dao;
import cl.ejemplo.model.Producto;

/**/
public class Main {
    public static void main(String[] args) {
    	ConexionBD conexionBD = new ConexionBD();
    	Connection conexion = conexionBD.establecerConexion();
    	
    	int accion = 0;
    	
        Scanner scanner = new Scanner(System.in);
        while (accion != 9)
        {
	        System.out.print("Introduce 1 - Select  | 2- Insert: | 3- Update: | 4- Delete: | 9- Salir: ");
	        accion = scanner.nextInt();
	        
	        if (accion == 1) {
		        if (conexion != null) {
		        	  Dao selectQuery= new Dao(conexion);
		              selectQuery.realizarConsulta();
		        }
		        accion = 0;
	        }else if(accion == 2) {
	        	 Producto producto = new Producto();
	        	  producto.setName("Dao");
	              producto.setPrice("9.990");
	              
	             Dao productoDao = new Dao(conexion); // Crea una instancia de ProductoDao
	             productoDao.insertarProducto(producto);
	             accion = 0;
	        } else if(accion == 3) {
	       	 	Producto producto = new Producto();
	       	 	producto.setId(3);
	       	 	producto.setName("DaoEdit2");
		        producto.setPrice("12.600");
		      // Crea una instancia de Dao
		        Dao productoDao = new Dao(conexion); 
		        productoDao.actualizarProducto(producto);
		        accion = 0;
	        }else if(accion == 4) {
		      // Crea una instancia de Dao
		        Dao productoDao = new Dao(conexion); 
		        productoDao.eliminarProducto(9); 
		        accion = 0;
	        }else if(accion == 9) {
	        	System.out.print("Adios!");
		    }
        }
    }
    
}
