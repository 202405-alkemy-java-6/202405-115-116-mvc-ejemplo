package cl.ejemplo.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cl.ejemplo.conexion.ConexionBD;
import cl.ejemplo.dao.Dao;


public class ProductoController extends HttpServlet{

	protected void doPost(HttpServletRequest request, HttpServletResponse response)  
            throws ServletException, IOException {  
        response.setContentType("text/html");  
        PrintWriter out=response.getWriter();  
        
        ConexionBD conexionBD = new ConexionBD();
    	Connection conexion = conexionBD.establecerConexion();
    	
		Dao dao = new Dao(conexion);
		request.setAttribute("productos",dao.obtenerTodos());  
		
		
		RequestDispatcher rd=request.getRequestDispatcher("products/viewAll.jsp");  
        rd.forward(request, response);  		

        
	}


    @Override  
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)  
            throws ServletException, IOException {  
        doPost(req, resp);  
    }  
	
}
